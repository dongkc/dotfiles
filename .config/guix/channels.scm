(list
 (channel
  (name 'nonguix)
  (url "https://gitlab.com/dongkc/nonguix")
  (branch "master"))

 (channel
  (name 'guix)
  (url "https://mirror.guix.org.cn/git/guix.git")
  (branch "master")))
