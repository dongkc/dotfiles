;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((scheme-mode
  .
  ((eval . (progn
             (require 'geiser-guile)
             (add-to-list 'geiser-guile-load-path
                          (locate-dominating-file buffer-file-name ".dir-locals.el")))))))
