;; To collect the size of a system:
;; guix size $(guix system -L ~/.config/guix/system build ~/.config/guix/system/default.scm)
(define-module (default)
  #:use-module (srfi srfi-1)
  #:use-module (guix packages)          ; For emacs-lucid
  #:use-module (guix build-system gnu)  ; For emacs-lucid
  #:use-module (guix utils)             ; For emacs-lucid
  #:use-module (gnu)
  #:use-module (gnu system nss)
  #:use-module (gnu system mapped-devices))

(use-service-modules
 desktop
 ;; For tor
 networking)

(use-package-modules
 certs                                  ; nss-certs
 linux                                  ; ntfs-3g
 mtools                                 ; exfat
 nano                                   ; To remove nano.
 emacs                                  ; For emacs-lucid
 emacs-xyz                              ; EXWM
 xorg                                   ; For emacs-lucid
 ;; To remove zile:
 zile)

(define %dongkai/cdemu-vhba-udev-rule
 ;; For the "uaccess" tag to be applied properly, the rule must be executed
 ;; before the uaccess rule
 ;; (/run/current-system/profile/lib/udev/rules.d/70-uaccess.rules).
  (udev-rule
   "69-cdemu-vhba.rules"
   (string-append "KERNEL==\"vhba_ctl\", SUBSYSTEM==\"misc\", TAG+=\"uaccess\"")))

;; Mount Nitrokey
;; TODO: Check if plugdev works instead of users.  If not, report to Nitrokey.
;; https://www.nitrokey.com/sites/default/files/41-nitrokey.rules
(define %nitrokey-udev-rule
  (udev-rule
   "41-nitrokey.rules"
   (string-append "ACTION==\"add\", SUBSYSTEM==\"usb\", "
                  "ATTR{idVendor}==\"20a0\", ATTR{idProduct}==\"4211\", "
                  "ENV{ID_SMARTCARD_READER}=\"1\", ENV{ID_SMARTCARD_READER_DRIVER}=\"gnupg\", GROUP+=\"users\", MODE=\"0666\"")))
;; (define %nitrokey-udev-rule
;;   (file->udev-rule
;;    "41-nitrokey.rules"
;;    (let ((version "20170910"))
;;      (origin
;;        (method url-fetch)
;;        (uri "https://www.nitrokey.com/sites/default/files/41-nitrokey.rules")
;;        (sha256
;;         (base32 "127nghkfd4dl5mkf5xl1mij2ylxhkgg08nlh912xwrrjyjv4y9sa"))))))

(define-public %dongkai/tor-config
  "ExitNodes {se},{nl},{fr},{ee},{no},{dk},{fi}
StrictNodes 1")

(define-public %dongkai/services
  (cons*
   (service tor-service-type
            (tor-configuration
             (config-file (plain-file "tor.conf" %dongkai/tor-config))))
  ;; Use the "desktop" services, which include the X11 log-in service, networking
  ;; with Wicd, and more.
  (modify-services
      %desktop-services
    (guix-service-type config =>
                       (guix-configuration
                        (inherit config)
                        ;; Don't clean build deps.
                        ;; See (info "(guix) Invoking guix-daemon").
                        ;; WARNING: This tends to yield an ever-growing store.
                        ;; (extra-options '("--gc-keep-outputs"))
                        ;; Specify substitutes manually.  Defaults
                        ;; should be good enough in most cases.
                        (substitute-urls
                         (list
                          "https://mirror.guix.org.cn"
                          "https://guix-mirro.pengmeiyu.com"))))
    (udev-service-type config =>
                       (udev-configuration
                        (inherit config)
                        (rules (append (udev-configuration-rules config)
                                       (list ;; %nitrokey-udev-rule
                                        %dongkai/cdemu-vhba-udev-rule))))))))

(define-public emacs-lucid
  (package
    (inherit emacs)
    (name "emacs-lucid")
    (synopsis "The Emacs text editor with Lucid support")
    (build-system gnu-build-system)
    (arguments
     (substitute-keyword-arguments (package-arguments emacs)
       ((#:configure-flags flags ''())
        `(cons "--with-x-toolkit=lucid" ,flags))
       ((#:phases phases)
        `(modify-phases ,phases
           (delete 'restore-emacs-pdmp)
           (delete 'strip-double-wrap)))))
    (inputs
     `(("libxaw" ,libxaw)
       ,@(alist-delete "gtk+" (package-inputs emacs))))))

(define-public emacs-lucid-xelb
   (package
     (inherit emacs-xelb)
     (name "emacs-lucid-xelb")
     (arguments
      (substitute-keyword-arguments (package-arguments emacs-xelb)
        ((#:emacs emacs) `,emacs-lucid)))))

(define-public emacs-lucid-exwm         ; No GTK, but the Slim service drags GTK+ anyways.
   (package
     (inherit emacs-exwm)
     (name "emacs-lucid-exwm")
     (synopsis "Emacs X window manager (using Lucid toolkit)")
     (propagated-inputs
      `(("emacs-lucid-xelb" ,emacs-lucid-xelb)))
     (arguments
      (substitute-keyword-arguments (package-arguments emacs-exwm)
        ((#:emacs emacs) `,emacs-lucid)))))

(define-public %dongkai/packages
  (cons* nss-certs          ; for HTTPS access
         ;; gvfs            ; TODO: For user mounts?
         ntfs-3g
         ;; exfat-utils     ; TODO: Needed for macOS drives?  Does not seem to work.
         fuse-exfat
         emacs-lucid-exwm   ; Still needs emacs-exwm / emacs-lucid-exwm installed in a user profile.
         vhba-module        ; For CDEmu.
         (fold (lambda (package l) (delete package l))
               %base-packages
               (list nano zile
                     ;; wireless-tools is deprecated in favour of iw.
                     wireless-tools))))

(define-public %dongkai/firmware
  %base-firmware)

(define-public %dongkai/user
  (user-account
   (name "dongkai")
   (group "users")
   (supplementary-groups '("wheel" "netdev" ; netdev is needed for networking.
                           "kvm"            ; For QEMU (and maybe libvirt)
                           ;; "plugdev"     ; TODO: Needed for nitrokey?
                           "lp"         ; TODO: Needed for bluetooth?
                           "video"))
   ;; TODO: Can we default to name?
   (home-directory "/home/dongkai")))

(define-public %dongkai/default-os
  (operating-system
    (host-name "dongkc-system")
    (timezone "Asia/Shanghai")
    (locale "en_US.utf8")

    ;; Use the UEFI variant of GRUB with the EFI System
    ;; Partition mounted on /boot/efi.
    (bootloader (bootloader-configuration
                 (bootloader grub-bootloader)
                 (timeout 1)
                 (target "/dev/sda")))

    (firmware %dongkai/firmware)
    ;; TODO: Remove all virtio modules?
    ;; (initrd-modules (delete "virtio-rng" %base-initrd-modules))

    (file-systems (cons*
                   (file-system
                     (device (file-system-label "guix"))
                     (mount-point "/")
                     (type "ext4"))
                   %base-file-systems))

    (users (cons* %dongkai/user
                  %base-user-accounts))

    (packages %dongkai/packages)

    (services %dongkai/services)

    ;; Allow resolution of '.local' host names with mDNS.
    (name-service-switch %mdns-host-lookup-nss)))

%dongkai/default-os
