;; With Eshell:
;; *sudo -E guix system -L ~/.config/guix/system reconfigure ~/.config/guix/system/laptop-mimimi.scm
(define-module (laptop-mimimi)
  #:use-module (srfi srfi-1)
  #:use-module (default)
  #:use-module (nongnu packages linux)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu)
  #:use-module (gnu system)
  #:use-module (gnu services)
  #:use-module (guix download) ; For linux-xiaomi-air-13
  #:use-module ((guix utils) :prefix guix-utils:)
  #:use-module (guix packages))

(use-service-modules
 desktop                                ; GNOME
 ;; GDM:
 xorg)

(use-package-modules
 gnome                                  ; gnome-minimal
 linux                                  ; For custom kernel.
 ;; For Xorg modules:
 xorg)

(define linux-xiaomi-air-13
  (package
    (inherit linux)
    (name "linux-xiaomi-air-13")
    ;; To build a custom kernel, pass it an alternate "kconfig":
    (native-inputs
     `(("kconfig" ,(local-file "./linux-laptop.conf"))
       ,@(alist-delete "kconfig" (package-native-inputs linux-libre))))))

(define %mimimi/power-tweaks
  ;; TODO: The following service starts too soon and results in a kernel panic
  ;; because /sys/... is not found.
  (simple-service
   'my-/sys-tweaks activation-service-type
   ;; >> echo '1' > '/sys/module/snd_hda_intel/parameters/power_save';
   #~(call-with-output-file "/sys/module/snd_hda_intel/parameters/power_save"
       (lambda (port)
         (display "1" port)))
   ;; >> echo 'auto' > '/sys/bus/usb/devices/1-6/power/control';
   ;; >> echo 'auto' > '/sys/bus/usb/devices/1-7/power/control';
   ;; >> echo 'auto' > '/sys/bus/i2c/devices/i2c-2/device/power/control';
   ;; >> echo 'auto' > '/sys/bus/pci/devices/0000:02:00.0/power/control';
   ))

(define %mimimi/xorg-touchpad
  "Section \"InputClass\"
  Identifier \"Touchpads\"
  Driver \"libinput\"
  MatchDevicePath \"/dev/input/event*\"
  MatchIsTouchpad \"on\"

  Option \"DisableWhileTyping\" \"on\"
  Option \"MiddleEmulation\" \"on\"
  Option \"ClickMethod\" \"clickfinger\"
  Option \"ScrollMethod\" \"twofinger\"
  Option \"NaturalScrolling\" \"true\"
EndSection")

(define %mimimi/backlight-udev-rule
  ;; Allow members of the "video" group to change the screen brightness.
  (udev-rule
   "90-backlight.rules"
   (string-append "ACTION==\"add\", SUBSYSTEM==\"backlight\", "
                  "RUN+=\"/run/current-system/profile/bin/chgrp video /sys/class/backlight/%k/brightness\""
                  "\n"
                  "ACTION==\"add\", SUBSYSTEM==\"backlight\", "
                  "RUN+=\"/run/current-system/profile/bin/chmod g+w /sys/class/backlight/%k/brightness\"")))

(define %mimimi/services
  (cons*
   ;; %dongkai/power-tweaks
   (modify-services
       %dongkai/services
     (udev-service-type config =>
                        (udev-configuration
                         (inherit config)
                         (rules (append (udev-configuration-rules config)
                                        (list %mimimi/backlight-udev-rule)))))
     (gdm-service-type config =>
                       (gdm-configuration
                        (inherit config)
                        (default-user "dongkai")
                        ;; (auto-login? #t)
                        (xorg-configuration
                         (xorg-configuration
                          (modules (list xf86-video-vesa
                                         xf86-video-intel
                                         ;; xf86-video-nouveau ; Make sure this is on if the 'nouveau' kernel module is loaded.
                                         xf86-input-libinput))
                          (extra-config (list %mimimi/xorg-touchpad)))))))))

(define-public gnome-minimal
  (package
    (inherit gnome)
    (name "gnome-minimal")
    (propagated-inputs
     ;; Keep nautilus.
     (fold alist-delete (package-propagated-inputs gnome)
           '(;; GNOME-Core-Shell
             "gnome-backgrounds"
             "gnome-themes-extra"
             "gnome-getting-started-docs"
             "gnome-user-docs"
             ;; GNOME-Core-Utilities
             "baobab"
             "cheese"
             "eog"
             "epiphany"
             "evince"             ; TODO: Remains pulled in with gnome-default-applications.
             "file-roller"
             "gedit"
             "gnome-boxes"
             "gnome-calculator"
             "gnome-calendar"
             "gnome-characters"
             "gnome-clocks"
             "gnome-contacts"
             "gnome-disk-utility"
             "gnome-font-viewer"
             "gnome-maps"
             ;; "gnome-music"
             ;; "gnome-photos"
             "gnome-screenshot"
             ;; "gnome-system-monitor" ; TODO: Needed for gnome-polkit-settings: edit package to no break when gnome-system-monitor is missing.
             "gnome-terminal"
             "gnome-weather"
             "simple-scan"
             "totem"
             ;; Others
             "gnome-online-accounts")))))

(define drive-mapping
  (list (mapped-device
         ;; The UUID is that returned by 'cryptsetup luksUUID'.
         (source (uuid "17d4df8c-04dc-43aa-b017-f69cd3bc5a7d"))
         (target "guix")
         (type luks-device-mapping))))

(define root-partition
  (file-system
    (device (file-system-label "guix"))
    (mount-point "/")
    (type "btrfs")
    (options "subvol=rootfs,compress=zstd")
    (dependencies drive-mapping)))

(operating-system
  (inherit %dongkai/default-os)
  (host-name "mimimi")

  (kernel linux)
  ;; (kernel linux-xiaomi-air-13)
  ;; To disable the Nvidia, first make sure nouveau is not loaded then call
  ;;   sudo tee /proc/acpi/bbswitch <<<OFF
  ;; Check the result with
  ;;   sudo dmesg | grep bbswitch
  ;; It saves about 1h of battery.
  ;; TODO: Automate the bbswitch command when the Guix package is merged.
  (kernel-arguments '("modprobe.blacklist=nouveau"))

  (mapped-devices drive-mapping)

  (firmware (append (list iwlwifi-firmware)
                    %dongkai/firmware))

  (file-systems (cons* root-partition
                       ;; TODO: Separate boot partition (to avoid double LUKS
                       ;; password prompt) does not work:
                       ;; (file-system
                       ;;   (device (file-system-label "boot"))
                       ;;   (mount-point "/boot")
                       ;;   (type "ext4")
                       ;;   ;; (needed-for-boot? #t)
                       ;;   (dependencies (list root-partition))
                       ;;   )
                       (file-system
                         (device (uuid "4E30-891F" 'fat))
                         (mount-point "/boot/efi")
                         (type "vfat"))
                       (file-system
                         (mount-point "/tmp")
                         (device "none")
                         (type "tmpfs")
                         (check? #f))
                       %base-file-systems))

  (services (cons* (service gnome-desktop-service-type
                            (gnome-desktop-configuration
                             (gnome gnome-minimal)))
                   %mimimi/services)))
