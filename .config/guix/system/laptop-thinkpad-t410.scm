;; This is an operating system configuration template
;; for a "desktop" setup without full-blown desktop
;; environments.

(use-modules (gnu) (gnu system nss) (gnu packages emacs) (gnu packages vim) (gnu packages version-control))
(use-service-modules desktop)
(use-package-modules bootloaders certs ratpoison suckless wm)
(use-package-modules bootloaders certs ratpoison suckless wm xorg)


(use-modules (nongnu packages linux)
             (nongnu system linux-initrd))

(define %thinkpad/backlight-udev-rule
  ;; Allow members of the "video" group to change the screen brightness.
  (udev-rule
   "90-backlight.rules"
   (string-append "ACTION==\"add\", SUBSYSTEM==\"backlight\", "
                  "RUN+=\"/run/current-system/profile/bin/chgrp video /sys/class/backlight/%k/brightness\""
                  "\n"
                  "ACTION==\"add\", SUBSYSTEM==\"backlight\", "
                  "RUN+=\"/run/current-system/profile/bin/chmod g+w /sys/class/backlight/%k/brightness\"")))

(define %thinkpad/services
  (cons*
   ;; %dongkai/power-tweaks
   (modify-services
       %dongkai/services
     (udev-service-type config =>
                        (udev-configuration
                         (inherit config)
                         (rules (append (udev-configuration-rules config)
                                        (list %thinkpad/backlight-udev-rule)))))
     (gdm-service-type config =>
                       (gdm-configuration
                        (inherit config)
                        (default-user "dongkai")
                        ;; (auto-login? #t)
                        (xorg-configuration
                         (xorg-configuration
                          (modules (list xf86-video-vesa
                                         xf86-video-intel
                                         ;; xf86-video-nouveau ; Make sure this is on if the 'nouveau' kernel module is loaded.
                                         xf86-input-libinput))
                          (extra-config (list %thinkpad/xorg-touchpad)))))))))


(operating-system
  (kernel linux)
  (initrd microcode-initrd)
  (firmware (list linux-firmware))

  (host-name "dongkc")
  (timezone "Asia/Shanghai")
  (locale "en_US.utf8")

  ;; Use the UEFI variant of GRUB with the EFI System
  ;; Partition mounted on /boot/efi.
  (bootloader (bootloader-configuration
                (bootloader grub-bootloader)
                (target "/dev/sda")))

  ;; Assume the target root file system is labelled "my-root",
  ;; and the EFI System Partition has UUID 1234-ABCD.
  (file-systems (append
                 (list (file-system
                         (device "/dev/sda1")
                         (mount-point "/")
                         (type "ext4")))
                 %base-file-systems))

  (users (cons (user-account
                (name "dongkc")
                (comment "Kai")
                (group "users")
                (supplementary-groups '("wheel" "netdev"
                                        "audio" "video")))
               %base-user-accounts))

  ;; Add a bunch of window managers; we can choose one at
  ;; the log-in screen with F1.
  (packages (append (list
                     ;; window managers
                     ratpoison i3-wm i3status dmenu
                     ;; terminal emulator
                     xterm
emacs
vim
git
                     ;; for HTTPS access
                     nss-certs)
                    %base-packages))

  ;; Use the "desktop" services, which include the X11
  ;; log-in service, networking with NetworkManager, and more.
  ;;(services %desktop-services)
  (services (modify-services %desktop-services
			     (guix-service-type
			       config => (guix-configuration
					   (inherit config)
					   (substitute-urls '("https://mirror.guix.org.cn"))))))

  ;; Allow resolution of '.local' host names with mDNS.
  (name-service-switch %mdns-host-lookup-nss))
