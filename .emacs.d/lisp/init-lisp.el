;;; Lisp

(require 'init-lispy)

(defun ambrevar/enhance-imenu-lisp (&rest keywords)
  "Add define-KEYWORD to `lisp-imenu-generic-expression'."
  (dolist (keyword keywords)
    (add-to-list
     'lisp-imenu-generic-expression
     (list (purecopy (concat (capitalize keyword) "s"))
           (purecopy (concat "^\\s-*("
                             (eval-when-compile
                               (regexp-opt
                                (list (concat "define-" keyword))
                                t))
                             "\\s-+\\(" lisp-mode-symbol-regexp "\\)"))
           2))))
;; Following defines are common, e.g. in Next.
(ambrevar/enhance-imenu-lisp "mode" "command" "parenscript")

;; For cl-dbus.
(add-to-list
 'lisp-imenu-generic-expression
 (list (purecopy "D-Bus")
       (purecopy (concat "^\\s-*("
                         (eval-when-compile
                           (regexp-opt
                            (list "dbus:define-dbus-method")
                            t))
                         "\\s-+(\\([^)]+\\)"))
       2))

(add-hook 'lisp-mode-hook 'ambrevar/turn-on-complete-filename)
(add-hook 'lisp-mode-hook 'ambrevar/turn-on-tab-width-to-8) ; Because some existing code uses tabs.
(add-hook 'lisp-mode-hook 'ambrevar/turn-off-indent-tabs)   ; Should not use tabs.
(add-hook 'lisp-mode-hook 'ambrevar/init-lispy)
(when (fboundp 'rainbow-delimiters-mode)
  (add-hook 'lisp-mode-hook #'rainbow-delimiters-mode))

;; Read CLHS locally.
(or
 ;; Quicklisp package.
 (load "~/.quicklisp/clhs-use-local.el" 'noerror)
 ;; Unofficial Guix package (non-free license).
 (when (require 'clhs nil 'noerror)
   (clhs-setup)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Sly
(with-eval-after-load 'sly
  (setq sly-lisp-implementations
        '((sbcl ("sbcl" "--noinform"))
          (ccl ("ccl"))
          (ecl ("ecl"))))
  (with-eval-after-load 'sly-mrepl
    (setq sly-mrepl-history-file-name (expand-file-name "sly-mrepl-history" user-emacs-directory))
    (define-key sly-mrepl-mode-map (kbd "C-c M-o") 'sly-mrepl-clear-repl))

  (defun ambrevar/sly-load-reload-system ()
    (interactive)
    (funcall
     (if (sly-eval `(slynk-asdf:asdf-system-loaded-p ,(intern (sly-current-package))))
         #'sly-asdf-reload-system
       #'sly-asdf-load-system)
     (intern (sly-current-package))))
  (define-key lisp-mode-map (kbd "<f6>") 'ambrevar/sly-load-reload-system)

  (defun ambrevar/sly-colorize-buffer (str)
    (ansi-color-apply str))
  (add-hook 'sly-mrepl-output-filter-functions 'ambrevar/sly-colorize-buffer)

  (setq sly-command-switch-to-existing-lisp 'always)
  (add-hook 'sly-mrepl-hook #'ambrevar/init-lispy)
  (add-hook 'sly-mrepl-hook #'rainbow-delimiters-mode)
  ;; REVIEW: With Emacs 27 we can:
  ;; (customize-set-variable 'helm-completion-style 'emacs)
  ;; (add-to-list 'completion-styles 'backend)
  (when (require 'helm-sly nil 'noerror)
    ;; (add-hook 'sly-mrepl-hook #'helm-sly-disable-internal-completion)
    ;; TODO:  The above hangs Emacs 27, why?
    (when (require 'helm-company nil :noerror)
      (add-hook 'lisp-mode-hook #'company-mode)
      (define-key lisp-mode-map (kbd "<tab>") 'helm-company)
      (define-key lisp-mode-map (kbd "M-<tab>") 'helm-company)
      (defun ambrevar/sly-set-keys ()
        (define-key sly-mrepl-mode-map (kbd "<tab>") 'helm-company)
        (define-key sly-mrepl-mode-map (kbd "M-<tab>") 'helm-company))
      (add-hook 'sly-mrepl-hook #'ambrevar/sly-set-keys)
      (add-hook 'sly-mrepl-hook #'company-mode))))

(defun ambrevar/sly-switch-to-repl () ; TODO: Replace with `helm-defswitch'.
  (interactive)
  (require 'sly)
  (require 'helm-sly)
  (pcase (length sly-net-processes)
    (0 (sly))
    (_ (let ((output-buffer (sly-mrepl--find-buffer (sly-current-connection))))
         (cond
          ((and (eq (current-buffer) output-buffer)
                (require 'helm-sly nil 'no-error))
           (helm-sly-mini))
          (output-buffer
           (pop-to-buffer output-buffer))
          (t (sly nil nil t)))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SLIME
(defun ambrevar/slime-switch-to-repl () ; TODO: Replace with `helm-defswitch'.
  (interactive)
  (require 'slime)
  (pcase (length slime-net-processes)
    (0 (slime))
    (1 (if (and (eq (current-buffer) (slime-output-buffer))
                (require 'helm-slime nil 'no-error))
           (helm-slime-mini)
         (pop-to-buffer (slime-output-buffer))))
    (_ (if (require 'helm-slime nil 'noerror)
           (helm-slime-mini)
         (pop-to-buffer (slime-output-buffer))))))

(defun ambrevar/slime-rainbow-init ()
  (font-lock-mode -1)
  (rainbow-delimiters-mode)
  (font-lock-mode))

(with-eval-after-load 'slime
  ;; REVIEW: Fix issue https://github.com/slime/slime/issues/523:
  ;; Remove with SLIME 2.25.
  (setq slime-defpackage-regexp
        "^(\\(cl:\\|common-lisp:\\|uiop:\\)?\\(defpackage\\|define-package\\)\\>[ \t']*")

  (setq slime-lisp-implementations
        '((sbcl ("sbcl" "--noinform"))
          (ccl ("ccl"))
          (ecl ("ecl"))))
  (let ((slime-extra '(slime-fancy
                       ;; slime-banner
                       slime-mrepl
                       slime-xref-browser
                       ;; slime-highlight-edits ; A bit slow...
                       slime-sprof
                       slime-quicklisp
                       slime-asdf
                       slime-indentation)))
    ;; TODO: Fix slime-repl-ansi-color.
    ;; (when (require 'slime-repl-ansi-color nil t)
    ;;   (add-to-list 'slime-extra 'slime-repl-ansi-color)
    ;;   (setq slime-repl-ansi-color t))

    ;; slime-company should not be `require'd, see
    ;; https://github.com/anwyn/slime-company/issues/11.
    (when (ignore-errors (find-library-name "slime-company"))
      (add-to-list 'slime-extra 'slime-company))
    (define-key slime-editing-map (kbd "C-c C-d C-h") 'slime-documentation-lookup)
    (slime-setup slime-extra)
    (add-hook 'slime-repl-mode-hook 'ambrevar/init-lispy)
    (add-hook 'slime-repl-mode-hook #'ambrevar/slime-rainbow-init)))


(when (require 'helm-sly nil 'noerror)
  (global-helm-sly-mode)
  (add-to-list 'helm-source-names-using-follow "Lisp xrefs"))

(when (require 'helm-slime nil 'noerror)
  (global-helm-slime-mode)
  (add-to-list 'helm-source-names-using-follow "SLIME xrefs"))

;; REVIEW: Redshank is probably dead: merge those features into SLIME/Sly?
(with-eval-after-load 'redshank
  (setq redshank-accessor-name-function 'redshank-accessor-name/%)
  (define-key lisp-mode-map (kbd "C-x C-r c") 'redshank-defclass-skeleton)
  (define-key lisp-mode-map (kbd "C-x C-r s") 'redshank-asdf-defsystem-skeleton))

(provide 'init-lisp)
