#!/bin/sh

PORT=8080

usage () {
	cat <<EOF>&2
Usage: ${0##*/} NETWORK-ADDRESS [DESTINATION]

Fetched data from rsync NETWORK-ADDRESS to DESTINATION (defaults to current
directory).  Only size is used to compare files.
Protocol and port are optional: they can be specified by command line
parameters.

Options:

  -H:      Fetch from HTTP instead of rsync.
  -p:      Specify a port number (default: $PORT).
  -s:      Fetch single file (only in HTTP).

EOF
}

OPT_HTTP=false
OPT_SINGLE="--recursive"
while getopts ":hHp:s" opt; do
	case $opt in
	h)
		usage
		exit ;;
	H)
		OPT_HTTP=true ;;
	p)
		PORT="$OPTARG" ;;
	s)
		OPT_SINGLE="";;
	\?)
		usage
		exit 1 ;;
	esac
done

shift $(($OPTIND - 1))

[ $# -lt 1 ] && usage && exit 1
[ "$1" = "-h" ] && usage && exit
[ "$1" = "--" ] && shift

ADDRESS="$1"
OUTPUT="."
[ -n "$2" ] && OUTPUT="$2"

case "$ADDRESS" in
http*)
	OPT_HTTP=true;;
rsync*)
	OPT_HTTP=false;;
*)
	if $OPT_HTTP; then
		ADDRESS=http://$ADDRESS:$PORT
	else
		ADDRESS=rsync://$ADDRESS:$PORT
	fi
esac

mkdir -p "$OUTPUT"
if $OPT_HTTP; then
	wget --reject 'index.html*' $OPT_SINGLE --continue --content-disposition --compression=gzip --no-proxy "$ADDRESS" --directory-prefix="$OUTPUT"
else
	# We ignore timestamp by removing "-t" from "-a".  See man page.
	rsync -ivzzP -rlpgoD --size-only $ADDRESS/files "$OUTPUT"
fi
