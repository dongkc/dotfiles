(list (channel
        (name 'nonguix)
        (url "https://gitlab.com/nonguix/nonguix.git")
        (commit
          "8624048c592bea418a5e3c79b89d7dd43e641f4e"))
      (channel
        (name 'guix-gaming-games)
        (url "https://gitlab.com/guix-gaming-channels/games.git")
        (commit
          "1c1594ab7f6728656e004cab9f81b0a854592527"))
      (channel
        (name 'guix-gaming-duke-nukem-3d)
        (url "https://gitlab.com/guix-gaming-channels/duke-nukem-3d.git")
        (commit
          "ab13db32b35c4eb043a0f8d66b47c700373ff2a8"))
      (channel
        (name 'guix-gaming-quake-3)
        (url "https://gitlab.com/guix-gaming-channels/quake-3.git")
        (commit
          "25135247e5b3bef70f43c50692177f024b09512b"))
      (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (commit
          "29e621a0134841705c007917e9a107013c8b0adb")))
