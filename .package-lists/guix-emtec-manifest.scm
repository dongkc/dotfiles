(specifications->manifest
 '("clojure"
   "docker-cli"
   "hyperledger-iroha"
   "leiningen"
   "libreoffice"
   "icedtea"
   "icedtea:jdk"
   "postgresql"
   ;; "python-keras" ; Broken?
   "qemu"
   "virt-manager"))
