(list (channel
        (name 'nonguix)
        (url "https://gitlab.com/nonguix/nonguix.git")
        (commit
          "1b8d409975b1038dfd7e52ad3d5d3d58e1995807"))
      (channel
        (name 'guix-gaming-games)
        (url "https://gitlab.com/guix-gaming-channels/games.git")
        (commit
          "870ceb78c43bcd648117b508ce7f03877b679960"))
      (channel
        (name 'guix-gaming-duke-nukem-3d)
        (url "https://gitlab.com/guix-gaming-channels/duke-nukem-3d.git")
        (commit
          "4861a022c43b9e45f8845fd08948bf163604f534"))
      (channel
        (name 'guix-gaming-quake-3)
        (url "https://gitlab.com/guix-gaming-channels/quake-3.git")
        (commit
          "25135247e5b3bef70f43c50692177f024b09512b"))
      (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (commit
          "fa122ac4ca2bd4c87b216e2f478397bcde2c1d8f")))
