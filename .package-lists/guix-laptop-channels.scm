(list (channel
        (name 'nonguix)
        (url "https://gitlab.com/nonguix/nonguix.git")
        (commit
          "2146063dc1eeb88cee3fb644ddd42a3ecf0e624c"))
      (channel
        (name 'guix-gaming-games)
        (url "https://gitlab.com/guix-gaming-channels/games.git")
        (commit
          "8d918a9d20d953d54e9daffdfc410c12b190b848"))
      (channel
        (name 'guix-gaming-duke-nukem-3d)
        (url "https://gitlab.com/guix-gaming-channels/duke-nukem-3d.git")
        (commit
          "ab13db32b35c4eb043a0f8d66b47c700373ff2a8"))
      (channel
        (name 'guix-gaming-quake-3)
        (url "https://gitlab.com/guix-gaming-channels/quake-3.git")
        (commit
          "25135247e5b3bef70f43c50692177f024b09512b"))
      (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (commit
          "0d01a5a9d47781f63c7bea3f6065fb0bed9f07f8")))
