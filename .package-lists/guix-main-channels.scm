(list (channel
        (name 'nonguix)
        (url "https://gitlab.com/nonguix/nonguix.git")
        (commit
          "fbc8431a2a50fab872f7e9f3765e2b6c60ae656e"))
      (channel
        (name 'guix-gaming-games)
        (url "https://gitlab.com/guix-gaming-channels/games.git")
        (commit
          "4f3889a4a61844c63255582e0c614c1187fd8c4e"))
      (channel
        (name 'guix-gaming-duke-nukem-3d)
        (url "https://gitlab.com/guix-gaming-channels/duke-nukem-3d.git")
        (commit
          "ab13db32b35c4eb043a0f8d66b47c700373ff2a8"))
      (channel
        (name 'guix-gaming-quake-3)
        (url "https://gitlab.com/guix-gaming-channels/quake-3.git")
        (commit
          "25135247e5b3bef70f43c50692177f024b09512b"))
      (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (commit
          "59d452da401c375e7bd18d2260c2e42ee0d05b72")))
