(list (channel
        (name 'nonguix)
        (url "https://gitlab.com/nonguix/nonguix.git")
        (commit
          "30e93befe5b1e5c1295271cbb27d5d1a5e728f05"))
      (channel
        (name 'guix-gaming-games)
        (url "https://gitlab.com/guix-gaming-channels/games.git")
        (commit
          "870ceb78c43bcd648117b508ce7f03877b679960"))
      (channel
        (name 'guix-gaming-duke-nukem-3d)
        (url "https://gitlab.com/guix-gaming-channels/duke-nukem-3d.git")
        (commit
          "4861a022c43b9e45f8845fd08948bf163604f534"))
      (channel
        (name 'guix-gaming-quake-3)
        (url "https://gitlab.com/guix-gaming-channels/quake-3.git")
        (commit
          "25135247e5b3bef70f43c50692177f024b09512b"))
      (channel
        (name 'guix)
        (url "https://git.savannah.gnu.org/git/guix.git")
        (commit
          "e7b86a0d88760275afefa0c44a3c30602f80aac0")))
