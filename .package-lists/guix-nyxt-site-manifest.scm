(specifications->manifest
  '("gcc-toolchain"                    ; Needed to compile some Common Lisp FFI.
    "sbcl"

    "pandoc"
    ;; "fcgi"
    ;; "sqlite"

    "cl-alexandria"
    "cl-caveman"
    "cl-clack"
    ;; "cl-clack-handler-fastcgi"
    "cl-datafly"
    "cl-envy"
    "cl-flexi-streams"
    "cl-hunchentoot"
    "cl-lack"
    "cl-ppcre"
    "cl-ironclad"
    "cl-markup"
    "cl-mito"
    "cl-project"
    "cl-s-xml"
    "cl-str"
    "cl-sxql"
    "cl-uuid"))
